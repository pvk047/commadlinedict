const dict=require("./src/dict");
let word="good";


    let jArr=[];
    let synArr=[];
    let defArr=[];
    let targetWord;
    console.log("Preparing for your game..");
    dict.getPlayDetails().then(data=>{
        console.log(`Def: [${data[0][0]["partOfSpeech"]}] ${data[0][0].def}`);
        console.log(`Synonym: ${data[1].splice(0,1)[0]}`);
        targetWord=data[3];
        jArr=data[2];
        synArr=data[1];
        defArr=defArr.map((el)=>{
            return el.def;
        })
        console.log("Enter Your word: ");
        getuserput(checkAnswer)
    }).catch(err=>{
        console.log("Oops,Some thing went wrong..");
        console.log(err);
        process.exit(0);
    })
    function getuserput(cb){
        process.stdin.setEncoding('utf8');
        process.stdin.on('readable', () => {
            const chunk = process.stdin.read();
           
            if(chunk!=null){
                cb(chunk);
                process.stdin.pause();
            }
        })
    }
    let _wrongAnswer=false;
    let checkAnswer=(data)=>{
        if(!isNaN(parseInt(data))&&_wrongAnswer){
            _wrongAnswer=false;
            if(data==1){
                console.log("Please Enter your word : ");
                process.stdin.resume();
            }else if(data==2){
                console.log("Hint: \n");
                let hint=getHint();
                console.log(`${hint[1]}: ${hint[0]}`);
                console.log("Please Enter your word : ");
                process.stdin.resume();
            }else if(data==3){
                let word=targetWord;
                dict.getFullDetails(word)
                .then(data => {
                    console.log("\nWord: ", word.toUpperCase());
                    data[0]=JSON.parse(data[0]);
                    data[2]=JSON.parse(data[2]);
                    if (data[0] && data[0].length > 0) {
                        console.log(" \nDefinition(s):");
                        data[0].forEach((el, index) => {
                            console.log(`${index + 1}) [${el.partOfSpeech.toUpperCase()}] ${el.text}`);
                        });
                        
                    }
                    
                    if (data[1] && data[1].length > 0) {
                       
                        if(data[1][0]){
                                console.log(`\n${titleCase(data[1][0]["relationshipType"])}(s): \n ${data[1][0].words.join()} `);
                        }else{
        
                        }
                        if(data[1][1]){
                            console.log(`\n${titleCase(data[1][1]["relationshipType"])}(s):\n ${data[1][1].words.join()} `);
                        }else{
        
                        }
                    }
                    
                    if(data[2] && data[2].examples.length > 0){
                        console.log("\nExamples:");
                       data[2].examples.forEach((el,index)=>{
                           console.log(`${index+1}) ${el.text}\n`);
                       })
                    }
                    process.exit();
                }).catch(err=>{
                    console.log(err);
                })
               
            }else{
                _wrongAnswer=true;
                console.log("please select a valid Option..");
                process.stdin.resume();
            }
        }else{
            if(synArr.indexOf(data.trim().toLowerCase())>-1 || data.trim().toLowerCase()== targetWord ){
                console.log("You Entered a Corrrect Answer...");
                process.exit(0);
            }else{
                _wrongAnswer=true;
                console.log("Oops,wrong Answer");
                console.log("Please Enter : \n 1--> To Try Again \n 2 --> For hints \n 3 --> See the result ");
                process.stdin.resume();
              
            }
        }
    }
    function getHint(){
        let hintArr=[{"type":"Anagram","data":jArr}]
        if(synArr.length>0){
            hintArr.push({"type":"Synonym",data:synArr});
        }
        if(defArr.length>0){
            hintArr.push({"type":"Definition","data":defArr});
        }
        let randomCursor=Math.floor(Math.random() * (hintArr.length-1));
        let randomIndex=Math.floor(Math.random() * (hintArr[randomCursor].data.length-1));
        let returnWord=hintArr[randomCursor]['data'].splice(randomIndex,1);
        if(hintArr[randomCursor]['data'].length==0){
            hintArr.splice(randomCursor,1);
        }
        return [returnWord[0],hintArr[randomCursor]['type']];
    }


/*dict.getWordOfTheDay().then(data=>{
    console.log("\nWord: ",data.word);
    console.log("\nDefinitions: ");
    data.definitions.forEach((el,index)=>{
        console.log(`\n${index+1}) [${el.partOfSpeech}] ${el.text}`)
    })
    console.log("\nExamples: ");
    data.examples.forEach((el,index)=>{
        console.log(`\n${index+1}) ${el.text}`)
    })
}).catch(err=>{
    console.log(err);
})*/



/*function titleCase(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
}

dict.getFullDetails(word).then(data => {
    console.log("\nWord: ", word.toUpperCase());
    data[0]=JSON.parse(data[0]);
    data[2]=JSON.parse(data[2]);
    if (data[0] && data[0].length > 0) {
        console.log(" \nDefinition(s):");
        data[0].forEach((el, index) => {
            console.log(`${index + 1}) [${el.partOfSpeech.toUpperCase()}] ${el.text}`);
        });
        
    }
    
    if (data[1] && data[1].length > 0) {
       
        if(data[1][0]){
                console.log(`\n${titleCase(data[1][0]["relationshipType"])}(s) \n: ${data[1][0].words.join()} `);
        }else{

        }
        if(data[1][1]){
            console.log(`\n${titleCase(data[1][1]["relationshipType"])}(s)\n: ${data[1][1].words.join()} `);
        }else{

        }
    }
    
    if(data[2] && data[2].examples.length > 0){
        console.log("\nExamples:");
       data[2].examples.forEach((el,index)=>{
           console.log(`${index+1}) ${el.text}\n`);
       })
    }
}).catch(err=>{
    console.log(err);
})*/


/*dict.ex(word).then(result=>{
    result = JSON.parse(result);

        if (result && result.examples.length > 0) {
            console.log("Word:", word);
            console.log("\nExamples:");
            result.examples.forEach((w, index) => {
                console.log(`${index + 1}) ${w.text}\n`);

            })

        } else {
            console.log("Word:", word);
            console.log("[-]","No Examples Found");
        }
}).catch(err=>{
    if (err.status == 500) {
        console.log("[-]","OOps,Something Went Wrong..");
    } else {
        console.log("[-]",err.msg);
    }
})*/



/*dict.related(word, 'synonym').then(result=>{
    // result = JSON.parse(result);
    // console.log(result);
    if (result.length > 0) {
        console.log("Word:", word);
        console.log("Synonyms:\n", result[0].words.join())
    } else {
        console.log("Word:", word);
        console.log("[-]","No Synonyms Found")
    }
}).catch(err=>{
    if (err) {
        if (err.status == 500) {
            console.log("[-]","OOps,Something Went Wrong..");
        } else {
            console.log("[-]",err);
        }
    }
})

dict.related(word, 'antonym').then(result => {
    if (result.length > 0) {
        console.log("Word:", word);
        console.log("Antonyms:\n", result[0].words.join())
    } else {
        console.log("Word:", word);
        console.log("[-]", "No Antonyms Found");
    }
}).catch(err => {
    if (err.status == 500) {
        console.log("[-]", "OOps,Something Went Wrong..");
    } else {
        console.log("[-]", err);
    }
})
*/

/*dict.def(word).then(result=>{
    result = JSON.parse(result);
    if(result.length>0){
        console.log("Word: ",word);
        console.log("\nDefinition(s):");
        result.forEach((el,index)=>{
            console.log(`${index+1}) [${el.partOfSpeech.toUpperCase()}] ${el.text}`);
        })
        console.log("\n");
    }else{
        console.log("Word: ",word);
        console.log("[-] No Definition Found...");
    }
    
}).catch(err=>{
    if (err.status == 500) {
        console.log("[-]","OOps,Something Went Wrong..");
    } else {
        console.log("[-]",err.msg);
    }
});*/