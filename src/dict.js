const config = require("config");
const request = require("request");

function titleCase(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
}

let getDef = function (word) {
    /**
     * @author Veerakrishna
     * @function getDef to get definition of a word from wordnik API
     * @param word {String} 
     */
    return new Promise((reslove, reject) => {
        if (word && word.length > 0) {
            let options = {
                method: 'GET',
                url: `https://api.wordnik.com/v4/word.json/${word}/definitions`,
                qs:
                {
                    limit: '5',
                    includeRelated: 'false',
                    useCanonical: 'false',
                    includeTags: 'false',
                    api_key: config.apikey
                }
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject({ "status": 500, "msg": error });
                }
                else {
                    reslove(body);
                }
            });
        } else {
            reject({ "status": 422, msg: "Please enter a word" });
        }
    })

}

let getRelated = function (word, type) {
  /**
   * @author Veerakrishna
   * @function getRelated to get synonyms,antonym and other realted words  of a word from wordnik API
   * @param word {String} 
   * @param type {String} Note:if you need multilple type then pass a string as 'type1,type2,..'
   */
    return new Promise((reslove, reject) => {
        if (word && word.length > 0) {
            let options = {
                method: 'GET',
                url: `https://api.wordnik.com/v4/word.json/${word}/relatedWords`,
                qs:
                {
                    limit: '200',
                    includeRelated: 'false',
                    useCanonical: 'false',
                    includeTags: 'false',
                    api_key: config.apikey
                }
            };
            if (type) {
                options['qs']['relationshipTypes'] = type;
            }
            request(options, function (error, response, body) {
                if (error) {
                    console.log(options);
                    console.log(error.stack);
                    reject({ "status": 500, "msg": error });
                }
                else {
                    try{
                        reslove(JSON.parse(body));
                    }catch(err){
                        console.log(err);
                        reject({ "status": 500, "msg": error });
                    }
                   
                }
            });
        } else {
            reject({ "status": 422, msg: "Please enter a word" });
        }
    })

}

let getExamples = function (word) {
    /**
     * @author Veerakrishna
     * @function getExamples to get example sentences of a word from wordnik API
     * @param word {String} 
     */
    return new Promise((reslove, reject) => {
        if (word && word.length > 0) {
            let options = {
                method: 'GET',
                url: `https://api.wordnik.com/v4/word.json/${word}/examples`,
                qs:
                {
                    includeDuplicates: 'false',
                    useCanonical: 'false',
                    api_key: config.apikey,
                    "limit": 5
                }
            };
            request(options, function (error, response, body) {
                if (error) {
                    reject({ "status": 500, "msg": error });
                }
                else {
                    reslove(body)
                }
            });
        } else {
            reject({ "status": 422, msg: "Please enter a word" });
        }
    })

}

let getFullDetails = function (word) {
    /**
    * @author Veerakrishna
    * @function getFullDetails to get defintions,synonyms,antonyms and examples of a word
    * @param word {String} 
    */
   return new Promise((reslove, reject) => {

       if (word && word.length > 0) {
           Promise.all([getDef(word), getRelated(word, "synonym,antonym"), getExamples(word)]).then(fullData => {
               fullData[1] = fullData[1].filter((el) => {
                   return (el["relationshipType"] == "synonym" || el["relationshipType"] == "antonym")
               });
               reslove(fullData);
           }).catch(err => {
               reject(err);
           })
       }
       else {
           reject({ "status": 422, msg: "Please enter a word" }, null);
       }
   })
}

let getWordOfTheDay = function () {
    /**
     * @author Veerakrishna 
     * @function getWordOfTheDay is usedto word of the day from wordnik API
     */
    return new Promise((reslove, reject) => {
        let today=new Date();
        today=today.getFullYear()+"-"+( (today.getMonth()+1) > 9 ? (today.getMonth()+1) : "0"+(today.getMonth()+1)  ) +"-"+( today.getDate() > 9 ?  today.getDate() : "0"+ today.getDate() );
        let options = {
            method: 'GET',
            url: "https://api.wordnik.com/v4/words.json/wordOfTheDay",
            qs:
            {
                date:today,
                api_key: config.apikey
            }
        };
        request(options, function (error, response, body) {
            if (error) {
                reject({ "status": 500, "msg": error });
            }
            else {

                reslove(JSON.parse(body));
            }
        });
    })
}
let getHelp= function(){
    /**
     * @author Veerakrishna 
     * @function getHelp is used to display some help
     */
    console.log("Command Line Dictionary Tool\n");
    console.log("DESCRIPTON:");
    console.log("   It is commandLine Dictionary tools based on  wordnik API");
    console.log("\nSynopsis : node index [<option>,<word>] ");
    console.log("\nOptions:\n");
    console.log("1) def:\n ");
    console.log("   Synopsis: node index def <word>");
    console.log("   Used to get Defintion(s) of a given word");
    console.log("\n2) syn:\n ");
    console.log("   Synopsis: node index syn <word>");
    console.log("   Used to get synonym(s) of a given word");
    console.log("\n3) ant:\n ");
    console.log("   Synopsis: node index ant <word>");
    console.log("   Used to get antonym(s) of a given word");
    console.log("\n4) ex:\n ");
    console.log("   Synopsis: node index ex <word>");
    console.log("   Used to get example(s) of a given word");
    console.log("\n5) play:\n ");
    console.log("   Synopsis: node index play");
    console.log(`   This displays a definition, synonym, or antonym
	And ask the user to enter the word

	If correct word is entered, You win the Game
	If incorrect word is entered, Game ask for
		- 1. try again
			Lets user enter word again

		- 2. hint
			Display a hint, and let user enter word again
			Hint can be
                Display the word randomly jumbled (cat -> atc)
                OR Display another definition of the word
                OR Display another antonym of the word
                OR Display another synonym of the word
		-3 quit
			Display the word, its full dict, and quit
`);
console.log("\n6) dict:\n ");
console.log("   Synopsis: node index dict <word>");
console.log("   Used to display Definition(s),synonym(s),antonym(s) and example(s) of a given word");

console.log("\nNotes:")
console.log("1)If no option is specified but word in mentioned then it will consider option as dict");
console.log("2)If no option and word are specified then display Details of Dayofwords from wordnik");
}

let getRandomWord = function () {
    /**
    * @author Veerakrishna
    * @function getRandomWord to get RandomWord from wordnik API
    * @param word {String} 
    */
   return new Promise((reslove, reject) => {

       let options = {
           method: 'GET',
           url: "https://api.wordnik.com/v4/words.json/randomWord",
           qs:
           {
               hasDictionaryDef: 'true',
               excludePartOfSpeech: "noun",
               maxCorpusCount: -1,
               minDictionaryCount: 1,
               maxDictionaryCount: -1,
               minLength: 3,
               maxLength: -1,
               api_key: config.apikey,
               "limit": 5
           }
       };
       request(options, function (error, response, body) {
           if (error) {
               reject({ "status": 500, "msg": error });
           }
           else {

               
                if(body){
                    console.log(body);
                    try{reslove(JSON.parse(body).word.toLowerCase())}
                    catch(err){
                        console.log(err);
                        reject({ "status": 500, "msg": "Ooops,Some thing went wrong with API." });
                    }
                }else{
                    console.log(body);
                    reject({ "status": 500, "msg": "Ooops,Some thing went wrong with API." });
                }
               
           }
       });

   })
}
let getjumbledArr = function (string) {
   /**
    * @author Veerakrishna
    * @function getjumbledArr is used to get all anagrams of a word as array
    * @param string {String}
    */
   return permut(string)
   function permut(string) {
       if (string.length < 2) return string; // This is our break condition

       var permutations = []; // This array will hold our permutations

       for (var i = 0; i < string.length; i++) {
           var char = string[i];

           // Cause we don't want any duplicates:
           if (string.indexOf(char) != i) // if char was used already
               continue;           // skip it this time

           var remainingString = string.slice(0, i) + string.slice(i + 1, string.length); //Note: you can concat Strings via '+' in JS

           for (var subPermutation of permut(remainingString)) {
               permutations.push(char + subPermutation);
           }

       }
       return permutations;
   }
}
let getPlayDetails = function () {
   /**
    * @author Veerakrishna
    * @function getPlayDetails is used to get all Details need for game like definitons,synonym,anagrams of a random words
    */
   return new Promise((reslove, reject) => {
       let thenFunction = (word) => {
           getRelated(word, "synonym")
           .then((relatedResult) => {
               if (relatedResult.length > 0) {

                   getDef(word).then(defData => {
                       defData = JSON.parse(defData);
                       defData = defData.map(el => {
                           return { "partOfSpeech": el.partOfSpeech, "def": el.text }
                       });
                       let jArr = getjumbledArr(word);
                       jArr.splice(0, 1)
                       reslove([defData, relatedResult[0].words, jArr, word]);
                   }).catch(err => {
                        console.log("[-] Oops,Something went wrong with API,please try Again");
                   })


               } else {
                   //console.log("[*]Fetching a good word for you")
                   getRandomWord().then(thenFunction);
               }
           }).catch(err=>{
            console.log("[-] Oops,Something went wrong with API,please try Again");

           })

       };
       getRandomWord().then(thenFunction).catch(err=>{
           console.log("[-] Oops,Something went wrong with API,please try Again");
       })
   })


}

module.exports.def = getDef;
module.exports.related = getRelated;
module.exports.ex=getExamples;
module.exports.getFullDetails=getFullDetails;
module.exports.getWordOfTheDay=getWordOfTheDay;
module.exports.getHelp=getHelp;
module.exports.getPlayDetails=getPlayDetails;