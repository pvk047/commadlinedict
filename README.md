**Command Line Dictionary Tool**

**DESCRIPTON**:
   
  	 It is commandLine Dictionary tool based on  wordnik API

**Synopsis** : 
	
		node index [<options>,<word>]

**Options**:

1) def:
 
	   Synopsis: node index def <word>

	   Used to get Defintion(s) of a given word

2) syn:
 
	   Synopsis: node index syn <word>

	   Used to get synonym(s) of a given word

3) ant:
 
	   Synopsis: node index ant <word>

	   Used to get antonym(s) of a given word

4) ex:
 
	   Synopsis: node index ex <word>

	   Used to get example(s) of a given word

5) play:
 
	   Synopsis: node index play

	   This displays a definition, synonym, or antonym
		And ask the user to enter the word

		If correct word is entered, You win the Game
		If incorrect word is entered, Game ask for
			- 1. try again
				Lets user enter word again

			- 2. hint
				Display a hint, and let user enter word again
				Hint can be
					Display the word randomly jumbled (cat -> atc)
					OR Display another definition of the word
					OR Display another antonym of the word
					OR Display another synonym of the word
			-3 quit
				Display the word, its full dict, and quit


6) dict:
 
	   Synopsis: node index dict <word>

	   Used to display Definition(s),synonym(s),antonym(s) and example(s) of a given word

**Notes**:

	1)If no option is specified but word in mentioned then it will consider option as dict

	2)If no option and word are specified then display Details of Dayofwords from wordnik
